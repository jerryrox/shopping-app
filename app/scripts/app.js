'use strict';

/**
* @ngdoc overview
* @name shoppingApp
* @description
* # shoppingApp
*
* Main module of the application.
*/
angular
.module('shoppingApp', [
  'ui.router',
  'ngSanitize',
  'ngMaterial',
  'md.data.table',
  'LocalStorageModule'
])
.config(function ($stateProvider, $urlRouterProvider, $mdThemingProvider, localStorageServiceProvider) {

  //setting the theme
  $mdThemingProvider.theme('default')
  .primaryPalette('teal');

  //set the prefix for local storage
  localStorageServiceProvider
    .setPrefix('shoppingApp');

  $stateProvider
  .state('productList', {
    url: '/product-list',
    controller: 'ProductlistCtrl',
    templateUrl: 'views/product-list.html'
  })
  .state('cart', {
    url: '/cart',
    controller: 'CartCtrl',
    templateUrl: 'views/cart.html'
  });

  $urlRouterProvider.otherwise('/product-list');

});
