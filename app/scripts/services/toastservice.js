'use strict';

/**
* @ngdoc service
* @name shoppingApp.toastService
* @description
* # toastService
* Service in the shoppingApp.
*/
angular.module('shoppingApp')
.service('toastService', function ($mdToast) {

  function show(text, delay, position) {
    position = position || 'bottom right';
    delay = delay || 3000; //ms
    $mdToast.show(
      $mdToast.simple()
      .textContent(text)
      .position(position)
      .hideDelay(delay)
    );
  }

  return {
    show: show
  };
});
