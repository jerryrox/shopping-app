'use strict';

/**
 * @ngdoc service
 * @name shoppingApp.productService
 * @description
 * # It is responsbile for calling the API that deals with product information
 */
angular.module('shoppingApp')
  .service('productService', function ($http, $q, config, localStorageService) {

    //incase of a real backend api
    var productApiURL = config.apiURL + '/product';

    function getProducts() {
      return $http.get('data/payload.json');
    }

    function filterProducts(products, ids) {
      var result = {};
      angular.forEach(products, function(product, key) {
        angular.forEach(product.items, function(item) {
          if(ids.indexOf(item.id) > -1) {
            if(!result.hasOwnProperty(item.secondaryCategory)) {
              result[item.secondaryCategory] = {};
            }
            result[item.secondaryCategory][item.id] = item;
          }
        })
      });
      return result;
    }

    function getProductsByIds(ids) {
      return $q(function(resolve, reject) {
        getProducts().then(function(resp) {
          resolve(filterProducts(resp.data.products, ids))
        }, function(err) {
          reject(err);
        });
      });
    }

    function getCart() {
      var savedCart = localStorageService.get('cart');
      if(savedCart) {
        return angular.fromJson(savedCart);
      }
    }

    function updateCart(cart, cb) {
      localStorageService.set('cart', angular.toJson(cart));
      if(cb) {
        cb();
      }
    }

    var productService = {
      getProducts: getProducts,
      getProductsByIds: getProductsByIds,
      getCart: getCart,
      updateCart: updateCart
    };
    return productService;

  });
