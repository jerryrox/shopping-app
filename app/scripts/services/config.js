'use strict';

/**
 * @ngdoc service
 * @name shoppingApp.config
 * @description
 * # Used to configure the backend api url.
 * Constant in the shoppingApp.
 */
angular.module('shoppingApp')
  .constant('config', {
    apiURL: 'http://localhost/api'
    //Grunt plugin can be used to configure dev and prod environments if required
  });
