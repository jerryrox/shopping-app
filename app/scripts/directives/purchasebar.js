'use strict';

/**
 * @ngdoc directive
 * @name shoppingApp.directive:purchaseBar
 * @description Displays the quanity of the total items purchased and the total cost
 * # purchaseBar
 */
angular.module('shoppingApp')
  .directive('purchaseBar', function () {
    return {
      templateUrl: 'views/purchase-bar.html',                
      restrict: 'E',
      scope: {
        totalProducts: '=',
        totalCost: '='
      }
    };
  });
