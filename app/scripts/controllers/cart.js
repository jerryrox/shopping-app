'use strict';

/**
* @ngdoc function
* @name shoppingApp.controller:CartCtrl
* @description
* # CartCtrl
* Controller of the shoppingApp
*/
angular.module('shoppingApp')
.controller('CartCtrl', function ($scope, productService, toastService) {

  $scope.getItemCount = getItemCount;
  $scope.updateQuantity = updateQuantity;
  $scope.removeItem = removeItem;

  $scope.data = {
    cartItems: null,
    totalCost: 0,
    totalProducts: 0,
    isCartEmpty: false
  };

  var cart = null;

  function getCart() {
    cart = productService.getCart();
    if(!cart || cart.totalProducts == 0) {
      toastService.show('No items in the cart!');
      $scope.data.isCartEmpty = true;
    } else {
      initData();
    }
  }

  getCart();

  function initData() {
    getProductsByIds(Object.keys(cart.items));
    $scope.data.totalCost = cart.totalCost;
    $scope.data.totalProducts = cart.totalProducts;
  }

  function getProductsByIds(ids) {
    productService.getProductsByIds(ids).then(function(products) {
      processData(products);
    }, function(err) {
      toastService.show('Some error occurred. Try reloading...');
    });
  }

  //adds the quantity against each item that was retrieved from cart stored in LS
  function processData(products) {
    var cartItems = cart.items;
    angular.forEach(products, function(items, category) {
      angular.forEach(items, function(item) {
        item['quantity'] = cart.items[item.id];
      });
    });
    $scope.data.cartItems = products;
  }

  //invoked from view to display total count under each category
  function getItemCount(product) {
    return Object.keys(product).length;
  }

  function updateQuantity(item, cartForm) {
    
    if(cartForm[item.id].$error.min || cartForm[item.id].$error.max || typeof item.quantity != "number") {
      toastService.show("Quantity must be between 1 and 100.");
      return;
    }

    var oldQuantity = cart['items'][item.id];
    cart['items'][item.id] = item.quantity;

    var quantityDiff = item.quantity - oldQuantity;
    var newCost = item.unitCost * item.unitsInCartons * quantityDiff;
    $scope.data.totalCost = cart.totalCost = cart.totalCost + newCost;
    $scope.data.totalProducts = cart.totalProducts = cart.totalProducts + quantityDiff;
    productService.updateCart(cart, function() {
      toastService.show('Quantity updated successfully');
    });
  }

  function removeItem(item, category) {
    delete $scope.data.cartItems[category][item.id];

    if(Object.keys($scope.data.cartItems[category]).length == 0) {
      delete $scope.data.cartItems[category];
    }

    //remove item from cart
    delete cart['items'][item.id];

    var itemValue = item.quantity * item.unitCost * item.unitsInCartons;

    //update purchase bar
    cart.totalProducts = $scope.data.totalProducts = $scope.data.totalProducts - item.quantity;
    cart.totalCost = $scope.data.totalCost = $scope.data.totalCost - itemValue;

    productService.updateCart(cart, function() {
      toastService.show('Item removed from the cart');
    });

    if(Object.keys($scope.data.cartItems).length == 0) {
      $scope.data.isCartEmpty = true;
    }

  }


});
