'use strict';

/**
* @ngdoc function
* @name shoppingApp.controller:ProductlistCtrl
* @description
* # ProductlistCtrl
* Controller of the shoppingApp
*/
angular.module('shoppingApp')
.controller('ProductlistCtrl', function ($scope, productService, toastService) {
  $scope.addToCart = addToCart;
  $scope.data = {};
  var cart = {
    items: {},
    totalCost: 0,
    totalProducts: 0
    //items will go here
  }; //holds the items added

  function getAllProducts() {
    productService.getProducts().then(function(resp) {
      if(resp.data) {
        $scope.data.products = resp.data.products;
        initCart();
      }
    }, function(err) {
      //handle error
    });
  }

  getAllProducts(); //get products when view loads

  function initCart() {
    angular.merge(cart, productService.getCart());
    $scope.data.totalProducts = cart.totalProducts;
    $scope.data.totalCost = cart.totalCost;
  }

  function addToCart(product) {
    var cost = 0;
    angular.forEach(product.items, function(items, key) {
      if(!cart['items'].hasOwnProperty(items.id)) {
        cart['items'][items.id] = 1;
      } else {
        cart['items'][items.id] += 1;
      }
      cost += items.unitCost * items.unitsInCartons;
    });
    cart.totalProducts = $scope.data.totalProducts += product.items.length;
    cost = $scope.data.totalCost + cost;
    cart.totalCost = $scope.data.totalCost = cost;
    productService.updateCart(cart, function() {
      toastService.show('Item added to cart');
    });
  }
});
