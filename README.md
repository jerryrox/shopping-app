# shopping

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.16.0.

## Build & development

Run `grunt build` for building and `grunt serve` for preview. Once built you can simply copy the dist directory on your server.
